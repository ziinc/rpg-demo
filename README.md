# 2D RPG Game
```bash
# dependencies
mix deps.get
(cd ./assets; npm i)

# dev server
mix phx.server

# tests
mix test

# build docker image
make build
```
## Implementation
![](docs/architecture.drawio.svg)

The `Board` Genserver holds the state of the board, while `Hero` Genserver holds the state of each hero.

A DynamicSupervisor is used to create heros dynamically. There is one dynamic supervisor per Board, and it is not given its own name or separate module, as it is linked intrinsically to the Board (one cannot exist without the other).

It is architected this way so as to provide possible scaling pathways for a multi-board architecture. Currently, only one Board genserver runs node-wide, as defined under `application.ex`. Of course, this would not be scalable in a realistic situation. Hence, the Board abstraction is set up to be possibly spun up dynamically.

## Requirements

- [x] field in grid can be wall or empty tile.
  - [x] wall tiles should be scattered throughout grid
    - can be predefined
- [x] user can control a hero
  - [x] new heros spawn on a random walkable tile
  - [x] new heros are assigned a random name
  - [x] user can customize hero name
    - [x] (web) multi-users can control same hero of same name. i.e. if change name to an existing hero, can control that hero.
  - [x] hero can move onto empty tiles
  - [x] hero cannot move on wall tiles
- [x] different connected user controls different hero
  - [x] user can distinguish controlled hero from enemies
  - [x] user can move onto tiles that enemies are on
  - [x] (ui) user-controlled hero renders above enemies
  - [x] dead heroes can be distinguished from others.
- [x] heros can attack enemies
  - [x] can attack in 1 tile radius around, inclusive of tile standing on
  - [x] able to attack multiple enemies at once
  - [x] one hit kills enemy (i.e. heros immediately die)
  - [x] 5 second respawn, at random location
  - [x] cannot perform actions when dead
- [x] Implementation
  - [x] each hero represented by a genserver
    - [x] holds position on grid
    - [x] status (alive/dead)

