defmodule Rpg.BoardTest do
  use Rpg.GameCase
  alias Rpg.{Hero, Board}

  setup do
    on_exit(fn ->
      Board.reset()
    end)
  end

  test "get_state/0 retrieves state" do
    assert %Board{} = Board.get_state()
  end

  test "create_hero/0 creates a new hero at a random passable position" do
    board = Board.get_state()
    assert {:ok, %Hero{position: coord}} = Board.create_hero()
    refute Board.impassable?(board, coord)
    assert Board.occupied?(board, coord)
  end

  test "move_hero/0 ignores dead hero commands" do
    assert {:ok, %Hero{pid: pid, position: old_pos} = hero} = Board.create_hero()
    Hero.update(pid, status: :dead)
    {x, y} = old_pos
    Board.move_hero(hero, {x + 3, y + 3})
    assert %{position: new_pos} = Hero.get_state(pid)
    assert new_pos == old_pos
  end

  test "move_hero/0 ignores out of bounds" do
    assert {:ok, %Hero{pid: pid, position: old_pos} = hero} = Board.create_hero()
    {x, y} = old_pos
    Board.move_hero(hero, {x + 55, y + 55})
    assert %{position: new_pos} = Hero.get_state(pid)
    assert new_pos == old_pos
  end

  test "attack/0 performs a hero attack" do
    board = Board.get_state()
    coord = Board.random_passable(board)

    heros =
      for _ <- 1..5 do
        {:ok, hero} = Board.create_hero()
        Hero.update(hero.pid, position: coord)
        Hero.get_state(hero.pid)
      end

    attacker = Enum.random(heros)
    Board.attack(attacker)

    for h <- heros do
      h = Hero.get_state(h.pid)

      if h.pid == attacker.pid do
        assert h.status == :alive
      else
        assert h.status == :dead
      end
    end
  end
end
