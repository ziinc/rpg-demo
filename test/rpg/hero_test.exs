defmodule Rpg.HeroTest do
  use Rpg.GameCase
  alias Rpg.Hero

  setup do
    pid = start_supervised!({Hero, name: "Unknown", position: {1, 2}, created_by_pid: self()})
    {:ok, pid: pid}
  end

  test "get_state/0 retrieves hero state", %{pid: pid} do
    assert %Hero{} = Hero.get_state(pid)
  end

  test "update/1 updates a hero", %{pid: pid} do
    assert {:ok, %Hero{name: "testing", position: {2, 2}}} =
             Hero.update(pid, name: "testing", position: {2, 2})
  end
end
