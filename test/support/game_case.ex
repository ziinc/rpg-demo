defmodule Rpg.GameCase do
  @moduledoc """
  This module defines the test case to be used for a game instance
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # convenience functions
    end
  end

  setup _tags do
    :ok
  end
end
