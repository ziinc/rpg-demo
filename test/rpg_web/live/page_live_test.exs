defmodule RpgWeb.PageLiveTest do
  use RpgWeb.ConnCase

  import Phoenix.LiveViewTest

  setup do
    on_exit(fn ->
      Rpg.Board.reset()
    end)
  end

  test "disconnected and connected render", %{conn: conn} do
    {:ok, live, html} = live(conn, "/")
    assert html =~ "RPG Game"

    frag = render(live)
    assert frag =~ "RPG Game"
    # grid contains walls/empty tiles
    assert has_element?(live, "div.wall")
    assert has_element?(live, "div.empty")

    # spawns a hero, has a controlled class
    assert has_element?(live, "div.hero.you", "Unknown")
  end

  describe "new hero" do
    test "can rename hero", %{conn: conn} do
      {:ok, live, _html} = live(conn, "/")

      assert live
             |> element("form#name")
             |> render_submit(%{hero: %{name: "Renamed"}})

      # renames the hero
      refute has_element?(live, "div.hero", "Unknown")
      assert has_element?(live, "div.hero", "Renamed")
    end

    test "user can assume control of existing hero instance", %{conn: conn} do
      {:ok, _live1, _html} = live(conn, "/")
      {:ok, live2, _html} = live(conn, "/")
      assert has_element?(live2, "div.hero.you", "Unknown 2")
      assert has_element?(live2, "div.hero.enemy", "Unknown 1")

      assert live2
             |> element("form#name")
             |> render_submit(%{name: "Unknown 1"})

      # live2 assumes control of live1 hero
      # despawns 2nd hero created
      refute has_element?(live2, "div.hero", "Unknown 2")
      assert has_element?(live2, "div.hero", "Unknown 1")
    end
  end

  describe "control" do
    test "dead heros", %{conn: conn} do
      {:ok, live, _html} = live(conn, "/")

      board = Rpg.Board.get_state()
      [hero] = Rpg.Board.list_heros(board)
      Rpg.Board.kill_hero(hero)

      :timer.sleep(1000)
      # can be distinguished
      assert has_element?(live, "div.hero.dead")

      # dead hero does not change grid
      grid = live |> element("#grid") |> render()

      live
      |> element("button", "Up")
      |> render_click()

      new_grid = live |> element("#grid") |> render()
      assert grid == new_grid

      :timer.sleep(6000)
      # revives
      refute has_element?(live, "div.hero.dead")
    end

    test "hero can move to possible move locations", %{conn: conn} do
      {:ok, live, _html} = live(conn, "/")
      grid = live |> element("#grid") |> render()

      # make it unlikely to be blocked by impassable tiles
      live
      |> element("button", "Up")
      |> render_click()

      live
      |> element("button", "Up")
      |> render_click()

      live
      |> element("button", "Left")
      |> render_click()

      live
      |> element("button", "Left")
      |> render_click()

      # grid changes
      new_grid = live |> element("#grid") |> render()
      assert grid != new_grid
    end

    test "hero can attack", %{conn: conn} do
      {:ok, live1, _html} = live(conn, "/")
      {:ok, _live2, _html} = live(conn, "/")
      # move them near each other

      board = Rpg.Board.get_state()
      heros = Rpg.Board.list_heros(board)
      coord = Rpg.Board.random_passable(board)

      for hero <- heros do
        Rpg.Hero.update(hero.pid, position: coord)
      end

      live1
      |> element("button", "Attack")
      |> render_click()

      assert has_element?(live1, "div.hero.dead")
    end
  end
end
