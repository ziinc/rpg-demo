defmodule Rpg.Hero do
  @doc """
  Defines the Struct of a Hero. Attributes are quite self-explanatory.
  """
  use GenServer
  alias Rpg.{Hero, Board}

  @type t :: %__MODULE__{
          position: Board.coordinate(),
          name: String.t(),
          status: :alive | :dead,
          pid: pid(),
          created_by_pid: pid()
        }
  defstruct position: nil,
            name: nil,
            status: :alive,
            pid: nil,
            created_by_pid: nil

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts)
  end

  @spec get_state(pid()) :: %Hero{}
  def get_state(pid) do
    GenServer.call(pid, :get_state)
  end

  @doc """
  Updates the state of a hero.
  Accepts the following keys:
  - name
  - status
  - position

  ### Examples
    iex> update(pid, name: "Testing")
    {:ok, %Hero{name: "testing", ....}}
  """
  @spec update(pid(), list() | map()) :: {:ok, %Hero{}}
  def update(pid, attrs \\ []) do
    attrs = attrs |> Enum.into(%{}) |> Map.take([:name, :status, :position])
    GenServer.call(pid, {:update, attrs})
  end

  @impl true
  def init(initial \\ []) do
    state = struct(Hero, initial)
    :ok = sanity_checks(state)
    {:ok, %{state | pid: self()}}
  end

  @impl true
  def handle_call(:get_state, _sender, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:update, attrs}, _sender, state) do
    updated = Map.merge(state, attrs)
    {:reply, {:ok, updated}, updated}
  end

  # perform some sanity checks for initialization
  defp sanity_checks(hero) do
    cond do
      is_nil(hero.name) ->
        raise "Hero name needs to be provided"

      is_nil(hero.position) ->
        raise "Hero starting position needs to be provided"

      is_nil(hero.created_by_pid) ->
        raise "Created by Pid needs to be provided"

      true ->
        :ok
    end
  end
end
