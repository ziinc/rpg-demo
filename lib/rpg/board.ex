defmodule Rpg.Board do
  use GenServer
  alias Rpg.{Hero, Board}

  @moduledoc """
  An instance of a game board.

  Each board contains the following elements:
  - tiles (walkable, empty, non-empty)
  - heros

  The grid is represented as a list of list (essentially a matrix)

  Convenience functions are not implemented as genserver callbacks to allow for a separation of state-manipulation functions and non-state manipulation functions.
  This separation circumvents the "Genserver calling self" runtime error.
  """
  @type coordinate :: {integer(), integer()}
  @type t :: %Board{
          grid: %{
            max_x: integer(),
            max_y: integer(),
            impassable: [coordinate()]
          },
          heros: [%Hero{}]
        }

  defstruct grid: nil,
            sup_pid: nil,
            # set as nil, so that errors will be throw if internal functions try to iterate over it
            # should be only set using the `list_heros` func so that it is always in sync with genserver states
            heros: nil

  def start_link(args \\ []) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl true
  def init(_) do
    {:ok, sup_pid} = DynamicSupervisor.start_link(strategy: :one_for_one)
    {:ok, %Board{grid: gen_grid(), sup_pid: sup_pid}}
  end

  @doc """
  Retrieves the state of the local (node-wide) board.

  ### Examples
    iex> get_state()
    %Board{...}
  """
  @spec get_state() :: %Board{}
  def get_state() do
    GenServer.call(__MODULE__, :get_state)
  end

  @doc """
  Resets the board state. Syncronous.

  ### Examples
    iex> reset()
    :ok
  """
  @spec reset() :: :ok
  def reset() do
    GenServer.call(__MODULE__, :reset)
  end

  @doc """
  Creates a hero on the game board

  ### Examples
    iex> create_hero()
    %Board{...}
  """
  @spec create_hero() :: {:ok, %Hero{}}
  def create_hero() do
    GenServer.call(__MODULE__, :create_hero)
  end

  @doc """
  Kills a hero. Syncronous

  ### Examples
    iex> kill_hero(hero)
    {:ok, %Hero{}}
  """
  @spec kill_hero(%Hero{} | list(%Hero{})) :: {:ok, %Hero{}}
  def kill_hero(%Hero{} = hero), do: kill_hero([hero])

  def kill_hero(heros) when is_list(heros) do
    GenServer.call(__MODULE__, {:kill_hero, heros})
  end

  @doc """
  A given hero performs an attack. Syncronous

  ### Examples
    iex> attack(hero)
    :ok
  """
  @spec attack(%Hero{}) :: :ok
  def attack(hero) do
    GenServer.call(__MODULE__, {:attack, hero})
  end

  @doc """
  Moves a hero to a target coordinate. Syncronous

  Attmepts to move to an impassable tile will be ignored.

  Attempts to move out of the grid is ignroed.

  ### Examples
    iex> move_hero(hero, {1, 2})
    :ok
  """
  @spec move_hero(%Hero{}, coordinate()) :: :ok
  def move_hero(%Hero{} = hero, coord) do
    GenServer.call(__MODULE__, {:move_hero, hero, coord})
  end

  @impl true
  def handle_call(:get_state, _sender, state) do
    updated =
      state
      |> Map.put(:heros, list_heros(state))

    {:reply, updated, state}
  end

  @impl true
  def handle_call(:create_hero, {sender, _}, state) do
    # monitor the sender, if sender crashes, we want to termiante the hero
    Process.monitor(sender)

    args = [
      name: gen_new_name(state),
      position: random_passable(state),
      created_by_pid: sender
    ]

    {:ok, pid} = DynamicSupervisor.start_child(state.sup_pid, {Hero, args})
    # get the hero info
    hero = Hero.get_state(pid)
    {:reply, {:ok, hero}, state}
  end

  @impl true
  def handle_call(:reset, _sender, state) do
    state = %{state | grid: gen_grid()}

    # kill all heros
    for h <- list_heros(state), do: despawn_hero(state, h)

    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:kill_hero, [%Hero{} | _] = heros}, _sender, state) do
    names = for h <- heros, do: h.name
    # kill matching heros
    for h <- list_heros(state), h.name in names do
      do_kill(h)
    end

    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:move_hero, hero, {target_x, target_y} = target_coord}, _sender, state) do
    hero = Hero.get_state(hero.pid)

    in_bounds? = fn ->
      target_x > 0 and target_y > 0 and target_x <= state.grid.max_x and
        target_y <= state.grid.max_y
    end

    if target_coord not in state.grid.impassable and hero.status == :alive and
         in_bounds?.() == true do
      Hero.update(hero.pid, position: target_coord)
    end

    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:attack, hero}, _sender, state) do
    hero = Hero.get_state(hero.pid)

    if hero.status == :alive do
      heros = list_heros(state)
      {x, y} = hero.position

      affected_coords =
        for curr_x <- [x - 1, x, x + 1],
            curr_y <- [y - 1, y, y + 1] do
          {curr_x, curr_y}
        end

      for h <- heros, h.pid != hero.pid, h.position in affected_coords do
        do_kill(h)
      end
    end

    {:reply, :ok, state}
  end

  @impl true
  def handle_info({:revive_hero, %Hero{pid: pid}}, state) do
    Hero.update(pid, status: :alive)
    {:noreply, state}
  end

  # this callback monitors for exits from processes that created heros
  @impl true
  def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
    for h <- list_heros(state), pid == h.created_by_pid do
      despawn_hero(state, h)
    end

    {:noreply, state}
  end

  # kills a hero and queues a respawn
  defp do_kill(hero) do
    Hero.update(hero.pid, status: :dead)
    # queue message to revive after 5 sec
    Process.send_after(self(), {:revive_hero, hero}, 5_000)
  end

  # creates a 12x12 grid with 5 randomly placed impassable tiles.
  defp gen_grid do
    coords = gen_coordinates()
    impassable = Enum.take_random(coords, 5)

    %{
      max_x: 12,
      max_y: 12,
      impassable: impassable
    }
  end

  # creates a list of coordinate tuples
  defp gen_coordinates do
    for x <- 1..12, y <- 1..12 do
      {x, y}
    end
  end

  # generates a new unused hero name
  defp gen_new_name(board, counter \\ 1) do
    name = "Unknown #{counter}"

    if name_taken?(board, name) do
      gen_new_name(board, counter + 1)
    else
      name
    end
  end

  # CONVENIENCE FUNCTIONS
  @doc """
  Lists all heros for a given board.

  ### Examples
  iex> list_heros(board)
  [%Hero{...}, ...]
  """
  @spec list_heros(Board.t()) :: [%Hero{}]
  def list_heros(%Board{sup_pid: sup_pid}) do
    for {_id, child_pid, _type, _modules} <- DynamicSupervisor.which_children(sup_pid) do
      Hero.get_state(child_pid)
    end
  end

  @doc """
  Terminates a given hero and removes it from the DynamicSupervisor.

  ### Examples
  iex> despawn_hero(board, hero_pid)
  :ok
  iex> despawn_hero(board, %Hero{...})
  :ok
  """
  @spec despawn_hero(Board.t(), pid() | %Hero{}) :: :ok
  def despawn_hero(%Board{} = board, %Hero{pid: pid}), do: despawn_hero(board, pid)

  def despawn_hero(%Board{sup_pid: sup_pid}, pid) when is_pid(pid) do
    DynamicSupervisor.terminate_child(sup_pid, pid)
  end

  @doc """
  An impassable tile is defined as a tile that cannot be moved on
  """
  @spec impassable?(%Board{}, coordinate()) :: boolean()
  def impassable?(%Board{grid: grid}, coord) when is_tuple(coord), do: coord in grid.impassable

  @doc """
  An occupied tile is defined as an passable tile that has at least one hero on it.
  """
  @spec occupied?(%Board{}, coordinate()) :: boolean()
  def occupied?(%Board{} = board, coord) when is_tuple(coord) do
    heros = list_heros(board)
    occupied_coordinates = for h <- heros, do: h.position
    coord in occupied_coordinates
  end

  @doc """
  Gets a passable tile
  """
  @spec random_passable(%Board{}) :: coordinate()
  def random_passable(%Board{grid: grid} = board) do
    coord = {Enum.random(1..grid.max_x), Enum.random(1..grid.max_y)}

    if coord not in grid.impassable do
      coord
    else
      random_passable(board)
    end
  end

  @doc """
  Indicates if a name has already been taken in a given board.
  """
  @spec name_taken?(%Board{}, String.t()) :: boolean()
  def name_taken?(%Board{} = board, name) when is_binary(name) do
    heros = list_heros(board)
    Enum.any?(heros, &(&1.name == name))
  end
end
