defmodule RpgWeb.PageLive do
  use RpgWeb, :live_view
  alias Rpg.{Hero, Board}
  @impl true
  def render(assigns) do
    ~L"""
    <h1>RPG Game</h1>
    <%= f = form_for :hero, "#", [phx_submit: :rename, id: "name"] %>
      <%= label f, :name %>
      <%= text_input f, :name, value: @hero_name %>
      <%= error_tag f, :name %>
      <%= submit "Rename" %>
    </form>
    <button phx-click="move" phx-value-direction="up">Up</button>
    <button phx-click="move" phx-value-direction="down">Down</button>
    <button phx-click="move" phx-value-direction="left">Left</button>
    <button phx-click="move" phx-value-direction="right">Right</button>
    <button phx-click="attack">Attack</button>
    <section id="grid" style="grid-template-columns: repeat(<%= @board.grid.max_x %>, minmax(0, 1fr));">
      <%= for  y <- 1..@board.grid.max_y, x <- 1..@board.grid.max_x do %>
            <div class="tile" data-grid="<%=  "#{x},#{y}" %>">
              <%= case tile_status(@board, {x, y}) do %>
                <% :wall -> %>
                  <div class="wall"></div>
                <% {:occupied, heros} -> %>
                  <div>
                    <%= for hero <- heros do %>
                      <% #multiline elixir for readability
                        hero_classes = "hero"
                        hero_classes =  hero_classes <> " " <> if(hero.name == @hero_name, do: "you", else: "enemy")
                        hero_classes =  hero_classes <> " " <> if(hero.status == :dead, do: "dead", else: "")
                      %>
                      <div class="<%= hero_classes %>">
                        <span><%= hero.name %></span>
                      </div>
                    <% end %>
                  </div>
                <% _ -> %>
                  <div class="empty"></div>
              <% end %>
            </div>
        <% end %>
    </section>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(hero_name: nil)

    socket =
      if connected?(socket) do
        # start update loop
        Process.send_after(self(), :reload, 600)
        # create hero
        {:ok, hero} = Board.create_hero()

        assign(socket, hero_name: hero.name, hero_pid: hero.pid)
      else
        socket
      end

    socket = assign(socket, board: Rpg.Board.get_state())
    {:ok, socket}
  end

  # retrieves the tile status from the board.
  defp tile_status(%{grid: grid} = board, coord) when is_tuple(coord) do
    cond do
      coord in grid.impassable ->
        :wall

      Board.occupied?(board, coord) ->
        heros_in_tile = for h <- Board.list_heros(board), h.position == coord, do: h
        {:occupied, heros_in_tile}

      true ->
        :empty
    end
  end

  @impl true
  def handle_info(:reload, socket) do
    socket = assign(socket, :board, Board.get_state())
    Process.send_after(self(), :reload, 600)
    {:noreply, socket}
  end

  @impl true
  def handle_event("rename", %{"hero" => %{"name" => name}}, socket) do
    heros = Board.list_heros(socket.assigns.board)
    current_hero = Enum.find(heros, fn h -> h.name == socket.assigns.hero_name end)
    hero_with_same_name = Enum.find(heros, fn h -> h.name == name end)

    socket =
      if hero_with_same_name do
        # assume control of this hero
        # despawn the current hero
        Board.despawn_hero(socket.assigns.board, current_hero)
        # control the target hero
        assign(socket,
          hero_name: hero_with_same_name.name,
          board: Rpg.Board.get_state()
        )
      else
        # ok to rename
        pid = socket.assigns.hero_pid
        {:ok, hero} = Hero.update(pid, name: name)

        socket
        |> assign(
          hero_name: hero.name,
          board: Rpg.Board.get_state()
        )
      end

    {:noreply, socket}
  end

  @impl true
  def handle_event("move", %{"direction" => direction}, socket) do
    heros = Board.list_heros(socket.assigns.board)
    hero = Enum.find(heros, fn h -> h.name == socket.assigns.hero_name end)
    {x, y} = hero.position

    case direction do
      "up" ->
        Board.move_hero(hero, {x, y - 1})

      "down" ->
        Board.move_hero(hero, {x, y + 1})

      "left" ->
        Board.move_hero(hero, {x - 1, y})

      "right" ->
        Board.move_hero(hero, {x + 1, y})
    end

    socket = assign(socket, board: Rpg.Board.get_state())
    {:noreply, socket}
  end

  @impl true
  def handle_event("attack", _params, socket) do
    heros = Board.list_heros(socket.assigns.board)
    hero = Enum.find(heros, fn h -> h.name == socket.assigns.hero_name end)
    Board.attack(hero)
    socket = assign(socket, board: Rpg.Board.get_state())
    {:noreply, socket}
  end

  # @impl true
  # def handle_event("search", %{"q" => query}, socket) do
  #   case search(query) do
  #     %{^query => vsn} ->
  #       {:noreply, redirect(socket, external: "https://hexdocs.pm/#{query}/#{vsn}")}

  #     _ ->
  #       {:noreply,
  #        socket
  #        |> put_flash(:error, "No dependencies found matching \"#{query}\"")
  #        |> assign(results: %{}, query: query)}
  #   end
  # end

  # defp search(query) do
  #   if not RpgWeb.Endpoint.config(:code_reloader) do
  #     raise "action disabled when not in development"
  #   end

  #   for {app, desc, vsn} <- Application.started_applications(),
  #       app = to_string(app),
  #       String.starts_with?(app, query) and not List.starts_with?(desc, ~c"ERTS"),
  #       into: %{},
  #       do: {app, vsn}
  # end
end
