FROM elixir:1.11-slim AS builder
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        nodejs \
        git \
        npm && \
        mix local.rebar --force && \
        mix local.hex --force

WORKDIR /code

# This copies our app source code into the build container
COPY . .

RUN MIX_ENV=prod mix setup
RUN MIX_ENV=prod mix build

# From this line onwards, we're in a new image, which will be the image used in production
FROM elixir:1.11-slim

WORKDIR /opt/app

COPY --from=builder /code/_build/prod/rel/rpg ./

CMD trap 'exit' INT; bin/rpg start
