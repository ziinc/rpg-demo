# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :rpg, RpgWeb.Endpoint,
  server: true,
  url: [host: "localhost", path: "/rpg"],
  secret_key_base: "FLyKDR8sx+gRriHSEEvTvqsof2HWJp3tw+iT9ZlamA2syhJ7uR10Y3g6Y9cVkwaO",
  render_errors: [view: RpgWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Rpg.PubSub,
  live_view: [signing_salt: "jNS029U/"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
